﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
namespace DAL
{
    public class FileDOA
    {


        public static FileDTO getFileById(int id)
        {

            FileDTO dto = new FileDTO();
            
            String query = String.Format("select * from Files where Id={0} ", id);
            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);

                while (reader.Read())
                {
                   dto = FillDTO(reader);
               
                }

            }

          return dto;

        }
        public static List<FileDTO> getAllActiveFiles()
        {
            List<FileDTO> list = new List<FileDTO>();
            String query ="select * from Files where isActive=1";
            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);

                while (reader.Read())
                {
                    FileDTO dto = FillDTO(reader);
                    list.Add(dto);
                }

            }

            return list;
        }
        public static List<FileDTO> getFilesByParentFolderId(int id)
        {
            List<FileDTO> list = new List<FileDTO>();
            String query = String.Format("select * from Files where ParentFolderId={0} and isActive=1", id);
            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);

                while (reader.Read())
                {
                    FileDTO dto = FillDTO(reader);
                    list.Add(dto);
                }

            }

            return list;
        }
        public static int saveFile(FileDTO dto)
        {

            String query = String.Format("insert into Files (Name,ParentFolderId,FileExt,FileSizeKb,UploadedOn,isActive,CreatedBy) value ('{0}',{1},'{2}',{3},Now(),{4},{5}); select @@IDENTITY ", dto.Name, dto.ParentFolderId,dto.FileExt,dto.FileSizeKb, 1,dto.CreatedBy);
            using (DBHelper helper = new DBHelper())
            {
                var obj = helper.ExecuteScalar(query);
                return Convert.ToInt32(obj);
            }

        }
        private static FileDTO FillDTO(MySqlDataReader reader)
        {
            var dto = new FileDTO();
            dto.Id = reader.GetInt32(0);
            dto.Name = reader.GetString(1);
            dto.ParentFolderId = reader.GetInt32(2);
            dto.FileExt = reader.GetString(3);
            dto.FileSizeKb =reader.GetUInt32(4);
            dto.UploadedOn = reader.GetDateTime(5);
            dto.isActive = reader.GetBoolean(6);
            dto.CreatedBy = reader.GetInt32(7);
            return dto;
        }

        public static int deleteFileById(int id)
        {
            String query = "delete from Files where Id=" + id;
            using (DBHelper helper = new DBHelper())
            {
                var obj = helper.ExecuteQuery(query);
                return Convert.ToInt32(obj);
            }

        }
    }
}
