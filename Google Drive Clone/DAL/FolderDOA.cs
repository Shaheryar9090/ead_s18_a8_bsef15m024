﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
namespace DAL
{
   public static class FolderDOA
    {
       public static List<FolderDTO> getAllActiveFolders()
       {
           List<FolderDTO> list = new List<FolderDTO>();
           String query ="select * from Folders where isActive=1";
           using (DBHelper helper = new DBHelper())
           {
               var reader = helper.ExecuteReader(query);

               while (reader.Read())
               {
                   FolderDTO dto = FillDTO(reader);
                   list.Add(dto);
               }

           }

           return list;
       }
       public static List<FolderDTO> getFolderContentById(int id)
       {
           List<FolderDTO> list = new List<FolderDTO>();
           String query = String.Format("select * from Folders where ParentFolderId={0} && isActive=1", id);
           using (DBHelper helper = new DBHelper())
           {
               var reader = helper.ExecuteReader(query);

               while (reader.Read())
               {
                   FolderDTO dto = FillDTO(reader);
                   list.Add(dto);
               }
     
           }

           return list;
       }
       public static int saveFolder(FolderDTO dto)
       {
           String query = String.Format("insert into Folders (Name,ParentFolderId,CreatedOn,isActive,CreatedBy) value ('{0}',{1},Now(),{2},{3}); select @@IDENTITY ", dto.Name, dto.ParentFolderId, 1,dto.CreatedBy);
           using(DBHelper helper = new DBHelper())
           {
               var obj=helper.ExecuteScalar(query);
               return Convert.ToInt32(obj);
           }
     
       }
       public static int deleteFolderById(int id)
       {
           string query = "update Folders set isActive=0 where Id =" + id;
           using (DBHelper helper = new DBHelper())
           {
               var obj = helper.ExecuteQuery(query);
               return Convert.ToInt32(obj);
           }
       }
       public static FolderDTO getFolderById(int id)
       {
           String query = String.Format("select * from Folders where Id={0}",id);
           using (DBHelper helper = new DBHelper())
           {
               FolderDTO dto = new FolderDTO(); ;
               var reader = helper.ExecuteReader(query);

               while (reader.Read())
               {
                  dto = FillDTO(reader);
                   
               }
               return dto;  
           }

       }
       private static FolderDTO  FillDTO(MySqlDataReader reader)
       {
           var dto = new FolderDTO();
           dto.Id = reader.GetInt32(0);
           dto.Name = reader.GetString(1);
           dto.ParentFolderId = reader.GetInt32(2);
           dto.CreatedOn = reader.GetDateTime(3);
           dto.isActive = reader.GetBoolean(4);
           dto.CreatedBy = reader.GetInt32(5);
           return dto;
       }
    }
}
