﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace DAL
{
   public class UserDOA
    {
        public static bool ValidateUser(String pLogin, String pPassword)
        {
            var query = String.Format("Select * from Users Where Login='{0}' and Password='{1}'", pLogin, pPassword);

            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);

                bool flag = false ;

                if (reader.Read())
                {
                    flag = true;
                }

                return flag;
            }
        }

        public static UserDTO GetUserByLogin(string login)
        {

            var query = String.Format("Select * from Users Where Login='{0}'", login);

            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);

                UserDTO dto = null;

                if (reader.Read())
                {
                    dto = FillDTO(reader);
                }

                return dto;
            }
        }
        private static UserDTO FillDTO(MySqlDataReader reader)
        {
            var dto = new UserDTO();
            dto.Id = reader.GetInt32(0);
            dto.Name = reader.GetString(1);
            dto.Login = reader.GetString(3);
            dto.Password = reader.GetString(2);
            dto.Email = reader.GetString(4);

            return dto;
        }
    }


}
