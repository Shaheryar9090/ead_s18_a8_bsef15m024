﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using Entities;
using Microsoft.WindowsAPICodePack.Shell;
using System.Drawing;
using System.IO;
using System.Diagnostics;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;


namespace Google_Drive_Clone.Controllers
{
    public class FolderController : Controller
    {
        
        //
        // GET: /Folder/
        public  ActionResult ShowContents()
        {
              
            return View();
        }
        
        //public ActionResult getChildFolders(int id)
        //{
        //    var list = DAL.FolderDOA.getFolderContentById(id);
        //    var list2 = DAL.FileDOA.getFilesByParentFolderId(id);
        //    var d = new
        //    {
        //        data = list,
        //        files=list2
        //    };
        //    return Json(d, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public ActionResult saveFolder(Entities.FolderDTO dto)
        //{
         
        //    int val = DAL.FolderDOA.saveFolder(dto);

        //    var d = new
        //    {
        //        success = val,
        //    };
        //    return Json(d, JsonRequestBehavior.AllowGet);
        //}
     
        //[HttpPost]
        //public ActionResult SaveFile(Entities.FileDTO dto)
        //{
         
        //    String uniqueName = "";
        //    if (Request.Files["file"] != null)
        //    {
        //        var file = Request.Files["file"];
        //        dto.Name = file.FileName;
        //        dto.FileSizeKb = file.ContentLength / 1024;

        //        var ext = System.IO.Path.GetExtension(file.FileName);
        //        dto.FileExt = ext;
                
        //        var rootPath = Server.MapPath("~/UploadedFiles");
        //        var fileSavePath = System.IO.Path.Combine(rootPath, dto.Name);
        //        //Save the uploaded file to "UploadedFiles" folder
        //        file.SaveAs(fileSavePath);

        //       // String name = dto.Name.Replace(ext," ");
        //       //Saving Thumbnail
               
        //        var ThumbPath = Server.MapPath("~/Thumbnails");
            
        //        ShellFile shellFile = ShellFile.FromFilePath(fileSavePath);
        //        Bitmap shellThumb = shellFile.Thumbnail.ExtraLargeBitmap;            
        //        var savingPath = Path.Combine(ThumbPath, dto.Name+".bmp");
        //        shellThumb.Save(savingPath);
                    
        //    }
           

        //    int val = DAL.FileDOA.saveFile(dto);
        //    var d = new
        //    {
        //        success = val,
             
        //    };
        //    return Json(d,JsonRequestBehavior.AllowGet);
        //}     

        //[HttpGet]
        //public ActionResult DeleteFile(int id)
        //{

        //    int val = DAL.FileDOA.deleteFileById(id);

        //    var d = new
        //    {
        //        success = val,
        //    };
        //    return Json(d, JsonRequestBehavior.AllowGet);
            
        //}
        
        public FileResult DownloadFile()
        {
            String fileName=Request["fileName"].ToString(); 
             byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/UploadedFiles/"+fileName));
             return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);       
        }
    
        //public ActionResult DeleteFolder(int id)
        //{
        ////    int val = DAL.FolderDOA.deleteFolderById(id);
        //    f(id);
        //    int val=DAL.FolderDOA.deleteFolderById(id);
        //    var d = new
        //    {
        //        success = 1,
        //    };
        //    return Json(d, JsonRequestBehavior.AllowGet);
            
        //}
        private void f(int id)
        {
            var list = DAL.FolderDOA.getFolderContentById(id);
            if(list.Count==0)
            {
                return;
            }
            for(int i = 0;i<list.Count;i++)
            {
                f(list[i].Id);
                DAL.FolderDOA.deleteFolderById(list[i].Id);
            }
        }
        public FileResult GeneratePDF()
        {
            var FolderList = FolderDOA.getAllActiveFolders();
            var FilesList = FileDOA.getAllActiveFiles();
            PdfDocument document = new PdfDocument();
            document.Info.Title = "Google Drive Meta Data";
            // Create an empty page
            PdfPage page = document.AddPage();
            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);
            // Create a font
            XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
            // Draw the text
            gfx.DrawString("Meta Data", font, XBrushes.Black,
                new XRect(10, 10, page.Width, page.Height), XStringFormats.TopCenter);
            gfx.DrawString("Folders", font, XBrushes.Black, 10, 50);

            //writing folders
         font = new XFont("Verdana", 11, XFontStyle.Regular);
            int height = 90;
            for(int i=0;i<FolderList.Count;i++)
            {
                if(i%12==0 && i!=0)
                {
                    page = document.AddPage();
                    gfx = XGraphics.FromPdfPage(page);
                    height = 15;
                }
                gfx.DrawString("Name : "+FolderList[i].Name, font, XBrushes.Black, 10, height);
                height += 15;
                gfx.DrawString("Type : Folder", font, XBrushes.Black, 10, height);
                string parentName = FolderDOA.getFolderById(FolderList[i].ParentFolderId).Name;
                if (FolderList[i].ParentFolderId == 1)
                    parentName = "Root";
                height += 15;
                gfx.DrawString("Parent : "+parentName, font, XBrushes.Black, 10, height);
                height += 25;
            }
            //wrting files now
            var fontHeading = new XFont("Arial", 20);
            page = document.AddPage();
            gfx = XGraphics.FromPdfPage(page);
            gfx.DrawString("Files", fontHeading, XBrushes.Black, 10, 30);
            height = 60;
            for (int i = 0; i < FilesList.Count; i++)
            {
                if (i % 10 == 0 && i != 0)
                {
                    page = document.AddPage();
                    gfx = XGraphics.FromPdfPage(page);
                    height = 15;
                }
                gfx.DrawString("Name : " + FilesList[i].Name, font, XBrushes.Black, 10, height);
                height += 15;
                gfx.DrawString("Type : File", font, XBrushes.Black, 10, height);
                string parentName = FolderDOA.getFolderById(FilesList[i].ParentFolderId).Name;
                if (FilesList[i].ParentFolderId == 1)
                    parentName = "Root";
                height += 15;
                gfx.DrawString("Size : " + FilesList[i].FileSizeKb + " KB", font, XBrushes.Black, 10, height);
                height += 15;
                gfx.DrawString("Parent : " + parentName, font, XBrushes.Black, 10, height);

          
                height += 25;
            }
        

             string filename = "MetaData.pdf";
             var rootPath = Server.MapPath("~/UploadedFiles");
             var fileSavePath = System.IO.Path.Combine(rootPath,filename);
             document.Save(fileSavePath);

             byte[] fileBytes = System.IO.File.ReadAllBytes(fileSavePath);
             return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);    
        }
	}
}