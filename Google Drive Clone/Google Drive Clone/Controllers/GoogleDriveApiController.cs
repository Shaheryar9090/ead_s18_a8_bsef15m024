﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Microsoft.WindowsAPICodePack.Shell;
using System.Drawing;
using System.IO;
using System.Diagnostics;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;


using Entities;
using DAL;
namespace Google_Drive_Clone.Controllers
{
    public class GoogleDriveApiController : ApiController
    {
        [HttpGet]
        public int add()
        {
            return 10 + 20;
        }
        [HttpPost]
        public Object getChildFolders()
        {
            int id = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["id"]);
            int UserId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["UserId"]);
            var temp = DAL.FolderDOA.getFolderContentById(id);
            List<Entities.FolderDTO> list = new List<FolderDTO>();
            for(int i = 0;i<temp.Count;i++)
            {
                if(temp[i].CreatedBy==UserId)
                {
                    list.Add(temp[i]);
                }
            }


            var temp2 = DAL.FileDOA.getFilesByParentFolderId(id);
            List<Entities.FileDTO> list2 = new List<FileDTO>();

            for (int i = 0; i < temp2.Count; i++)
            {
                if (temp2[i].CreatedBy == UserId)
                {
                    list2.Add(temp2[i]);
                }
            }

            var d = new
            {
                data = list,
                files = list2
            };
            return d;
        }

        [HttpPost]
        public Object saveFolder()
        {

            FolderDTO dto = new FolderDTO();
            dto.Name=System.Web.HttpContext.Current.Request.Params["Name"];
           dto.CreatedBy = Convert.ToInt32(System.Web.HttpContext.Current.Request.Params["UserId"]);
            dto.ParentFolderId =Convert.ToInt32(System.Web.HttpContext.Current.Request.Params["ParentFolderId"]);
            int val = DAL.FolderDOA.saveFolder(dto);
            
            var d = new
            {
                success = val,
            };
            return d;
        }

        [HttpPost]
        public Object SaveFile()
        {
            String uniqueName = "";
            FileDTO dto = new FileDTO();
            dto.ParentFolderId = Convert.ToInt32(HttpContext.Current.Request.Params["ParentFolderId"]);
           dto.CreatedBy = Convert.ToInt32(HttpContext.Current.Request.Params["UserId"]);
            if (HttpContext.Current.Request.Files.Count>0)
            {
                var file = HttpContext.Current.Request.Files["file"];
                dto.Name = file.FileName;
                dto.FileSizeKb = file.ContentLength / 1024;

                var ext = System.IO.Path.GetExtension(file.FileName);
                dto.FileExt = ext;

                var rootPath = HttpContext.Current.Server.MapPath("~/UploadedFiles");
                var fileSavePath = System.IO.Path.Combine(rootPath, dto.Name);
                //Save the uploaded file to "UploadedFiles" folder
                file.SaveAs(fileSavePath);

                // String name = dto.Name.Replace(ext," ");
                //Saving Thumbnail

                var ThumbPath = HttpContext.Current.Server.MapPath("~/Thumbnails");

                ShellFile shellFile = ShellFile.FromFilePath(fileSavePath);
                Bitmap shellThumb = shellFile.Thumbnail.ExtraLargeBitmap;
                var savingPath = Path.Combine(ThumbPath, dto.Name + ".bmp");
                shellThumb.Save(savingPath);
            }
            int val = DAL.FileDOA.saveFile(dto);
            var d = new
            {
                success = val,

            };
            return d;
        }

        [HttpGet]
        public Object DeleteFile(int id)
        {

            int val = DAL.FileDOA.deleteFileById(id);

            var d = new
            {
                success = val,
            };
            return d;

        }
        
        [HttpGet]
        public Object DeleteFolder(int id)
        {
            //    int val = DAL.FolderDOA.deleteFolderById(id);
            f(id);
            int val = DAL.FolderDOA.deleteFolderById(id);
            var d = new
            {
                success = 1,
            };
            return d;

        }
        private void f(int id)
        {

            var list = DAL.FolderDOA.getFolderContentById(id);
            if (list.Count == 0)
            {
                return;
            }
            for (int i = 0; i < list.Count; i++)
            {
                f(list[i].Id);
                DAL.FolderDOA.deleteFolderById(list[i].Id);
            }
        }

    }
}