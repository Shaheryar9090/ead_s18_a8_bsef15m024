﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Google_Drive_Clone.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Login")]
        public ActionResult Login2()
        {
            String login = Request["Login"];
            String pass = Request["Password"];
          if(DAL.UserDOA.ValidateUser(login, pass))
          {
              Session["user"] = login;
              return Redirect(Url.Content("~/Home/Index"));
          }
          @ViewBag.error = "Wrong UserName Or Password";
            
            return View();
        }

        [HttpPost]
        public ActionResult ValidateUser()
        {
            String login = Request.Form["Login"].ToString();
            String pass = Request.Form["Password"].ToString() ;

            String a = Request.Form["Password"].ToString() ;
            if (DAL.UserDOA.ValidateUser(login, pass))
            {
                Session["user"] = login;
                Entities.UserDTO u = DAL.UserDOA.GetUserByLogin(login);
                var d = new
                {
                    UserId = u.Id,
                    UserLogin=u.Login,
                    success = true
                };
                return Json(d, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var fail = new
                {
                    success = false
                };
                return Json(fail, JsonRequestBehavior.AllowGet);

            }
        }
        public ActionResult LogOut()
        {
            Session["user"] = null;
            
            return View("Login");
        }

	}
}