﻿

    currentFolderId = [1];
    path = ["My Drive"];
    $(document).ready(MainFunction);
    function ShowFolderContent(id) {
        var body = $("#foldersBody");
        var filesBody = $("#filesBody");
        console.log(id);
        filesBody.html(' ');
        body.html(' ');
        var data;
        var settings = {
            type: "GET",
            url: '@Url.Content("~/Folder/getChildFolders/")' + id,
            contentType: false,
            processData: false,
            data: data,
            success: function (r) {

                for (var i = 0; i < r.data.length; i++) {
                    var s = $("<span>");
                    s.val();
                    s.text(r.data[i]["Name"]);
                    s.val(r.data[i]["Name"]);
                    s.attr("id", r.data[i]["Id"]);
                    s.attr("text", r.data[i]["Name"]);
                    s.addClass("folder");
                    var icon = $("<i>");
                    icon.addClass("material-icons");
                    icon.addClass("icon");
                    icon.text("folder");
                    s.append(icon);
                    s.dblclick(function () {
                        // var path = $("#currentPath");
                        //var t = path.text();
                        //path.text(t + "->" + this.innerText);
                        var t = this.innerText.replace("folder", "");
                        path.push(t);
                        t = path.toString().replaceAll(",", "->");
                        $("#currentPath").text(t);
                        ShowFolderContent(this.id);
                    })

                    s.contextmenu(function () {

                        var v = confirm("Do you want to delete " + this.value + " Folder ?");
                        if (v == true) {
                            deleteFolder(this.id);
                        }
                    })
                    body.append(s);

                }
                for (var j = 0 ; j < r.files.length; j++) {
                    var d = $("<div>");
                    d.addClass("myThumbnail");

                    var name = $("<div>");
                    name.text(r.files[j]["Name"]);
                    name.css("font-weight", "bold");

                    var img = $("<img>");
                    img.attr("src", '@Url.Content("~/Thumbnails/")' + r.files[j]["Name"] + '.bmp');
                    img.width(150);
                    img.height(150);
                    d.append(name);
                    d.append(img);
                    var b = $("<button>");
                    b.addClass("btn");
                    b.addClass("btn-default");
                    b.text("Download");
                    b.click(downloadFile);

                    var b2 = $("<button>");
                    b2.addClass("btn");
                    b2.addClass("btn-danger");
                    b2.text("Delete");
                    b2.click(deleteFile);



                    var d2 = $("<div>");
                    d2.attr("id", r.files[j]["Id"]);
                    d2.css("width", "inherit");
                    d2.append(b);
                    d2.append(b2);
                    d.append(d2);
                    filesBody.append(d);

                }


            },
            error: function () {
                alert('error has occurred');
            }
        };

        $.ajax(settings);
        forward(id);
    }
    function forward(id) {
        if (currentFolderId[currentFolderId.length - 1] != id)
            currentFolderId.push(parseInt(id));
        console.log(currentFolderId);
    }
    function navigateBack() {
        if (currentFolderId.length > 1) {
            path.pop();
            currentFolderId.pop();

            var t = path.toString().replaceAll(",", "->");
            $("#currentPath").text(t);
        }
        console.log(currentFolderId);
        ShowFolderContent(currentFolderId[currentFolderId.length - 1]);
    }

    function deleteFolder(id) {
        var dto;
        var settings = {
            type: "GET",
            url: '@Url.Content("~/Folder/DeleteFolder/")' + id,
            contentType: false,
            processData: false,
            data: dto,
            success: function (r) {
                alert("Folder Deleted");
                ShowFolderContent(getCurrentFolderId());
                HideFileDialog();
            },
            error: function () {
                alert('error has occurred');
            }
        };

        $.ajax(settings);
    }
    function getCurrentFolderId() {
        return currentFolderId[currentFolderId.length - 1];
    }
    function saveFolder() {
        var name = $("#savingName").val();
        var dto = new FormData();
        dto.append("Name", name);
        dto.append("ParentFolderId", currentFolderId[currentFolderId.length - 1])

        var settings = {
            type: "POST",
            url: '@Url.Content("~/Folder/saveFolder")',
            contentType: false,
            processData: false,
            data: dto,
            success: function (r) {

                ShowFolderContent(getCurrentFolderId());
                alert("Folder Added");
                HideDialog();
            },
            error: function () {
                alert('error has occurred');
            }
        };

        $.ajax(settings);
    }
    function saveFile() {

        var dto = new FormData();
        var files = $("#file").get(0).files;
        if (files.length > 0) {
            dto.append("file", files[0]);
        }
        dto.append("ParentFolderId", currentFolderId[currentFolderId.length - 1]);
        var settings = {
            type: "POST",
            url: '@Url.Content("~/Folder/saveFile")',
            contentType: false,
            processData: false,
            data: dto,
            success: function (r) {
                alert("File Added" + r.success);
                ShowFolderContent(getCurrentFolderId());
                HideFileDialog();
            },
            error: function () {
                alert('error has occurred');
            }
        };

        $.ajax(settings);
    }
    function downloadFile() {
        var id = $(this).closest('div').attr("id");
        var fileName = $(this).parent().parent().children(":first").text();

        window.location = '@Url.Content("~/Folder/DownloadFile?fileName=")' + fileName;
    }
    function deleteFile() {
        var id = $(this).closest('div').attr("id");
        var dto;
        var settings = {
            type: "GET",
            url: '@Url.Content("~/Folder/DeleteFile/")' + id,
            contentType: false,
            processData: false,
            data: dto,
            success: function (r) {
                alert("File Deleted");
                ShowFolderContent(getCurrentFolderId());
                HideFileDialog();
            },
            error: function () {
                alert('error has occurred');
            }
        };

        $.ajax(settings);

    }

    function downloadMetaData() {
        window.location = '@Url.Content("~/Folder/GeneratePDF")';
    }

    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };

    function MainFunction() {

        $("#btnSaveFile").click(function () {
            saveFile();

        })


        $("#btnBack").click(function () {
            navigateBack();
        })
        $("#downloadMetaData").click(function () {
            downloadMetaData();
        })
        $('#lnkShowDialog').click(function (event) {


            ShowDialog();
            //To stop actual/default functionality of the link
            event.preventDefault();
            return false;
        });

        $('#lnkClose').click(function (e) {
            HideDialog();
            e.preventDefault();
        });

        $('#btnOK').click(function (e) {

            saveFolder();
            e.preventDefault();
        });

        //File Dialog
        $("#FileDialogShow").click(function (event) {
            ShowFileDialog();
            event.preventDefault();
        })
        $("#btnCloseFileDialog").click(function (event) {
            HideFileDialog();
            event.preventDefault();
        })

        $("#search").keyup(function () {

            var value = $(this).val().toLowerCase();
            $("#foldersBody span").each(function () {
                console.log($(this).text().toLowerCase().indexOf(value) > -1);
                if (($(this).text().toLowerCase().indexOf(value) > -1) == true)
                    $(this).show();
                else
                    $(this).hide();

                //  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            })


            $("#filesBody div").each(function () {
                $(this).toggle($(this).eq(0).text().toLowerCase().indexOf(value) > -1);
            })

        })


        ShowFolderContent(getCurrentFolderId());

    } //End of MainFunction

    function ShowFileDialog() {

        $('#divoverlay').show();
        $('#file_modal_dialog').slideDown(500);
    }
    function HideFileDialog() {
        $('#divoverlay').hide();
        $('#file_modal_dialog').slideUp(500);
    }

    function ShowDialog() {

        $('#divoverlay').show();
        $("#savingName").val('');
        $('#modal_dialog').slideDown(500);
    }
    function HideDialog() {
        $('#divoverlay').hide();
        $('#modal_dialog').slideUp(500);
    }


