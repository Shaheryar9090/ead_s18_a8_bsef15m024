-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: imedico
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `add_id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `shop` varchar(30) DEFAULT NULL,
  `street` varchar(30) DEFAULT NULL,
  `c_id` varchar(30) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`add_id`),
  KEY `c_id` (`c_id`),
  CONSTRAINT `address_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `clinic` (`c_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'Pakistan','Lahore','1','Cantt','1','johar town'),(7,'Pakistan','lahore','56B','86','Clinicia07','anarkali'),(8,'Pakistan','lahore','shop # 56-A','street # 8','health909','anarkali'),(12,'Pakistan','multan','shop # 16-R','street # 8','heal10','model town'),(13,'Pakistan','karachi','shop # 16-R','street # 8','firstAid','Layari');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointment` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` varchar(30) DEFAULT NULL,
  `doc_id` varchar(30) DEFAULT NULL,
  `ap_date` date DEFAULT NULL,
  `p_id` varchar(30) DEFAULT NULL,
  `ap_start_time` varchar(30) DEFAULT NULL,
  `ap_status` varchar(20) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ap_id`),
  KEY `c_id` (`c_id`),
  KEY `doc_id` (`doc_id`),
  KEY `p_id` (`p_id`),
  CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `clinic` (`c_id`),
  CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`doc_id`) REFERENCES `doctor` (`doc_id`),
  CONSTRAINT `appointment_ibfk_3` FOREIGN KEY (`p_id`) REFERENCES `patient` (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES (4,'1','1','2018-10-10','rio9','09:30','pending','dumy'),(9,'heal10','34','2018-01-19','yolo8','09:30','pending','test case'),(11,'heal10','2','2018-01-18','yolo8','15:01','pending','check'),(19,'heal10','2','2018-10-19','yolo8','16:00','pending','noob'),(20,'heal10','2','2018-10-19','rio9','16:30','pending','Hello'),(21,'health909','1','2018-01-26','rio9','15:00','pending','my appointment'),(22,'Clinicia07','12','2018-10-24','rio9','15:00','pending','akdahd'),(23,'heal10','2','2018-08-31','rio9','15:00','pending','emmergency');
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic`
--

DROP TABLE IF EXISTS `clinic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic` (
  `c_name` varchar(30) DEFAULT NULL,
  `c_id` varchar(30) NOT NULL,
  `c_phone` varchar(30) DEFAULT NULL,
  `openTime` varchar(30) DEFAULT NULL,
  `closeTime` varchar(30) DEFAULT NULL,
  `speciality1` varchar(45) DEFAULT NULL,
  `speciality2` varchar(45) DEFAULT NULL,
  `c_password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic`
--

LOCK TABLES `clinic` WRITE;
/*!40000 ALTER TABLE `clinic` DISABLE KEYS */;
INSERT INTO `clinic` VALUES ('imedico','1','8645744','5:00 AM','11:00 PM','Brain','Stomach','123','Eg@gmail.com'),('Clinicia','Clinicia07','03217560984','7:10 PM','11:50 PM','Heart','Child','12345678','clinic@gmail.com'),('First Aid Clinic','firstAid','0194919486','13:00','23:00','child','misc','12345678','first@gmail.com'),('Heal Stop','heal10','02193710','9:15 AM','9:16 PM','Brain','Sugar','12345678','heal@gmail.com'),('Health Care','health909','03217560984','5:10 PM','10:50 PM','Lungs','Kidney','12345678','health@gmail.com');
/*!40000 ALTER TABLE `clinic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_doc`
--

DROP TABLE IF EXISTS `clinic_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_doc` (
  `id` int(11) DEFAULT NULL,
  `doc_id` varchar(10) NOT NULL,
  `c_id` varchar(10) NOT NULL,
  `doc_start_time` varchar(30) DEFAULT NULL,
  `doc_end_time` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`doc_id`,`c_id`),
  KEY `doc_id` (`doc_id`),
  KEY `c_id` (`c_id`),
  CONSTRAINT `clinic_doc_ibfk_1` FOREIGN KEY (`doc_id`) REFERENCES `doctor` (`doc_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `clinic_doc_ibfk_2` FOREIGN KEY (`c_id`) REFERENCES `clinic` (`c_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_doc`
--

LOCK TABLES `clinic_doc` WRITE;
/*!40000 ALTER TABLE `clinic_doc` DISABLE KEYS */;
INSERT INTO `clinic_doc` VALUES (1,'1','1','17:00','19:00'),(2,'1','health909','13:30','20:30'),(NULL,'12','Clinicia07','01:00','13:00'),(3,'2','heal10','13:00  ','22:30'),(5,'34','heal10','8:00','11:00');
/*!40000 ALTER TABLE `clinic_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_ratings`
--

DROP TABLE IF EXISTS `clinic_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_ratings` (
  `1stars` int(50) DEFAULT NULL,
  `2stars` int(50) DEFAULT NULL,
  `3stars` int(50) DEFAULT NULL,
  `4stars` int(50) DEFAULT NULL,
  `5stars` int(50) DEFAULT NULL,
  `c_id` varchar(30) DEFAULT NULL,
  KEY `c_id` (`c_id`),
  CONSTRAINT `clinic_ratings_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `clinic` (`c_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_ratings`
--

LOCK TABLES `clinic_ratings` WRITE;
/*!40000 ALTER TABLE `clinic_ratings` DISABLE KEYS */;
INSERT INTO `clinic_ratings` VALUES (6,4,5,3,4,'health909');
/*!40000 ALTER TABLE `clinic_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `doc_name` varchar(30) DEFAULT NULL,
  `doc_id` varchar(10) NOT NULL,
  `doc_phone` varchar(30) DEFAULT NULL,
  `doc_specialization` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES ('Ali Nasir','1','000','heart'),('DR. D','10','19863','Child'),('ali','11','18236863','Heart'),('Dr. strange','12','090078601','super natural'),('Shaheryar','2','9127310973','brain'),('dummy','3','1213','nothing'),('Shaakir','34','129310','SUGAR');
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `p_name` varchar(30) DEFAULT NULL,
  `p_id` varchar(30) NOT NULL,
  `p_gender` varchar(10) DEFAULT NULL,
  `p_phone` varchar(30) DEFAULT NULL,
  `p_email` varchar(30) DEFAULT NULL,
  `p_dob` date DEFAULT NULL,
  `p_password` varchar(45) DEFAULT NULL,
  `p_img` blob,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES ('Wasif','1','Male','000','000','2018-01-02',NULL,NULL),('Ali nasir','ali99','Male','03215907859','hello@gmail.com','2018-01-15','123123',NULL),('Shaheryar','rio9','Male','03215907859','hello@gmail.com','2018-01-17','123',NULL),('Usama Ubaid','Usama45','Female','03215907859','sample@gmail.com','2013-01-04','123456',NULL),('Memona','yolo8','Female','03328231453','sample@gmail.com','1996-12-25','asdzxc',NULL);
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-20 13:20:44
